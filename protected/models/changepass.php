<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of regisUser
 *
 * @author Phe
 */
class ChangePass extends CFormModel {

    public $oldpass;
    public $password;
    private $_identity;
    
    public function rules() {
        return array(
            array('oldpass, password', 'required'),
            array('oldpass, password', 'length', 'min' => '3', 'max' => '12'),
            array('oldpass', 'validateoldpass'),
        );
    }

    public function attributeLabels() {
        return array(
        );
    }

    public function validateoldpass() {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity(Yii::app()->user->name, $this->oldpass);
            if ($this->_identity->authenticate())
                $this->addError('oldpass', 'Old Password Invalid');
        }
    }
    
    public function changepassuser() {
        if ($this->_identity === null)
            $this->_identity = new UserIdentity(Yii::app()->user->name, $this->password);
        $this->_identity->changePass();
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $this->oldpass = "";
            $this->password = "";
            return true;
        } else {
            $this->addError('password', 'Error while insert data');
            return false;
        }
    }

}
