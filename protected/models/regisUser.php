<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of regisUser
 *
 * @author Phe
 */
class RegisUser extends CFormModel {

    public $username;
    public $password;
    public $level;
    private $_identity;

    public function rules() {
        return array(
            array('username, password, level', 'required'),
            array(
                'username',
                'match', 'not' => true, 'pattern' => '/[^a-zA-Z_-]/',
                'message' => 'Invalid characters in username.',
            ),
            array('username', 'length', 'min' => '3', 'max' => '12'),
            array('username', 'validateusername'),
        );
    }

    public function attributeLabels() {
        return array(
            'level' => 'Level',
        );
    }

    public function validateusername($username) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->username, $this->password);
            if ($this->_identity->registeredUser($this->username))
                $this->addError('username', 'Username already Exist.');
        }
    }
    
    public function registerUser() {
        if ($this->_identity === null)
            $this->_identity = new UserIdentity($this->username, $this->password);
        if ($this->level == 1)
            $this->level = 1337;
        $this->_identity->registerUsr($this->level);
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $this->username = "";
            $this->password = "";
            $this->level = "";
            return true;
        } else {
            $this->addError('level', 'Error while insert data');
            return false;
        }
    }

}
