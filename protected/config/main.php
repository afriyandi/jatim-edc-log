<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');


// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
        'timezone' => 'Asia/Jakarta',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'theme'=>'bootstrap',
        'defaultController' => 'site/index',
	'name'=>'EDC Log',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'pswd',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
                      'generatorPaths'=>array('bootstrap.gii',),
		),
		
	),

	// application components
	'components'=>array(
                'bootstrap'=>array(
                    'class'=>'bootstrap.components.Bootstrap',),
            
		'user'=>array(
                        'class' => 'application.components.ELevel',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
            
                'logedc' => array(
                    'class' => 'application.components.ParseLogEDC',
                ),
            
                'jaUser' => array(
                    'class' => 'application.components.UserRspAndreq'
                ),
            
                'jatimTrans' => array(
                    'class' => 'application.components.TransRspAndReq'
                ),
            
		// uncomment the following to enable URLs in path-format		
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
                                '<action:(contact|login|logout|index|register|logedc)>'=>'site/<action>',
                                '<view:[a-zA-Z0-9-]+>/'=>'site/page',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
    				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
                        'urlSuffix' => '.phe',
		),
		
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
		// uncomment the following to use a MySQL database
		'db'=>array(
//                        'class'=>'system.db.CDbConnection',
//                        'schemaCachingDuration'=>3600,
                        'enableParamLogging' => true,
			'connectionString' => 'mysql:host=localhost;dbname=dummy',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
            
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
//					'levels'=>'error, warning, trace, log',
					'levels'=>'error, warning',
//                                        'categories'=> 'system.db.CDbCommand',
                                        'logFile' => 'phe.log'
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'setiawan@friyandi.info',
	),
);