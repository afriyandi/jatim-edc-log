<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserRspAndreq
 *
 * @author Phe
 */
class UserRspAndreq extends ParseLogEDC {

    public $user_req;
    public $user_rsp;

    public function __construct() {
        parent::__construct();
        $this->user_req = parent::getUserReq();
        $this->user_rsp = parent::getUserRsp();
        $this->userAct();
//        $this->format_data();
    }

    private function userAct() {

        //i don't want to join req and resp, due to ga ada kepastian tiap request pasti ada respon apa ga.
        if (isset($this->user_rsp) || isset($this->user_req)) {
            foreach ($this->user_req as $key => $value) {
                switch ($this->user_req[$key][3]) {
                    case 440000:
                        $this->user_req[$key]['act'] = 'ADD USER';
                        break;
                    case 460000:
                        $this->user_req[$key]['act'] = 'UPDATE USER';
                        break;
                    case 450000:
                        $this->user_req[$key]['act'] = 'DELETE USER';
                        break;
                    case 470000:
                        $this->user_req[$key]['act'] = 'VERIFIKASI USER';
                        break;
                    case 480000:
                        $this->user_req[$key]['act'] = 'LOGIN USER';
                        break;
                }
                if (isset($this->user_req[$key]['70'])) {
                    switch ($this->user_req[$key]['70']) {
                        case 001:
                            $this->user_req[$key]['act'] = 'Sign-on';
                            break;
                        case 002:
                            $this->user_req[$key]['act'] = 'Sign-off';
                            break;
                        case 301:
                            $this->user_req[$key]['act'] = 'Echo Test';
                            break;
                    }
                }
                $this->user_req[$key]['id'] = $key+1;
            }
            foreach ($this->user_rsp as $key => $value) {
                switch ($this->user_rsp[$key][3]) {
                    case 440000:
                        $this->user_rsp[$key]['act'] = 'ADD USER';
                        break;
                    case 460000:
                        $this->user_rsp[$key]['act'] = 'UPDATE USER';
                        break;
                    case 450000:
                        $this->user_rsp[$key]['act'] = 'DELETE USER';
                        break;
                    case 470000:
                        $this->user_rsp[$key]['act'] = 'VERIFIKASI USER';
                        break;
                    case 480000:
                        $this->user_rsp[$key]['act'] = 'LOGIN USER';
                        break;
                }
                if (isset($this->user_rsp[$key]['70'])) {
                    switch ($this->user_rsp[$key]['70']) {
                        case 001:
                            $this->user_rsp[$key]['act'] = 'Sign-on';
                            break;
                        case 002:
                            $this->user_rsp[$key]['act'] = 'Sign-off';
                            break;
                        case 301:
                            $this->user_rsp[$key]['act'] = 'Echo Test';
                            break;
                    }
                }
                $this->user_rsp[$key]['id'] = $key+1;
            }
        }
    }

    function format_data() {
        foreach ($this->user_req as $key => $value) {
            foreach ($value as $id => $data) {
                $this->user_req['id'] = $key;
                $this->user_req[$id] = $data;
            }
        }
    }

}
