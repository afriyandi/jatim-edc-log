<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransRspAndReq
 *
 * @author Phe
 */
class TransRspAndReq extends ParseLogEDC {

    public $tra_req;
    public $tra_rsp;

    public function __construct() {
        parent::__construct();
        $this->tra_req = parent::getTransReq();
        $this->tra_rsp = parent::getTransRsp();
        $this->userAct();
//        $this->format_data();
    }

    private function userAct() {

        //i don't want to join req and resp, due to ga ada kepastian tiap request pasti ada respon apa ga.
        if (isset($this->tra_rsp) || isset($this->tra_req)) {
            foreach ($this->tra_req as $key => $value) {
                switch ($this->tra_req[$key][3]) {
                    case 130000:
                        $this->tra_req[$key]['act'] = 'PINPAD REQUEST';
                        break;
                    case 261000:
                        $this->tra_req[$key]['act'] = 'INFO SALDO TABUNGAN';
                        break;
                    case 262000:
                        $this->tra_req[$key]['act'] = 'INFO SALDO GIRO';
                        break;
                    case 611000:
                        $this->tra_req[$key]['act'] = 'TARTUN TABUNGAN';
                        break;
                    case 612000:
                        $this->tra_req[$key]['act'] = 'TARTUN GIRO';
                        break;
                    case 621000:
                        $this->tra_req[$key]['act'] = 'INQ. SETUN TABUNGAN';
                        break;
                    case 622000:
                        $this->tra_req[$key]['act'] = 'INQ. SETUN GIRO';
                        break;
                    case 741000:
                        $this->tra_req[$key]['act'] = 'SETUN TABUNGAN';
                        break;
                    case 742000:
                        $this->tra_req[$key]['act'] = 'SETUN GIRO';
                        break;
                    case 221000:
                        $this->tra_req[$key]['act'] = 'INQ. TRANS TABUNGAN';
                        break;
                    case 222000:
                        $this->tra_req[$key]['act'] = 'INQ. TRANS GIRO';
                        break;
                    case 721000:
                        $this->tra_req[$key]['act'] = 'TRANSFER TABUNGAN';
                        break;
                    case 722000:
                        $this->tra_req[$key]['act'] = 'TRANSFER GIRO';
                        break;
                    case 271000:
                        $this->tra_req[$key]['act'] = 'MINI STAT. TABUNGAN';
                        break;
                    case 272000:
                        $this->tra_req[$key]['act'] = 'MINI STAT. GIRO';
                        break;
                    case 120000:
                        $this->tra_req[$key]['act'] = 'GANTI PIN';
                        break;
                }
                $this->tra_req[$key]['id'] = $key+1;
            }
            foreach ($this->tra_rsp as $key => $value) {
                switch ($this->tra_rsp[$key][3]) {
                    case 130000:
                        $this->tra_rsp[$key]['act'] = 'PINPAD REQUEST';
                        break;
                    case 261000:
                        $this->tra_rsp[$key]['act'] = 'INFO SALDO TABUNGAN';
                        break;
                    case 262000:
                        $this->tra_rsp[$key]['act'] = 'INFO SALDO GIRO';
                        break;
                    case 611000:
                        $this->tra_rsp[$key]['act'] = 'TARTUN TABUNGAN';
                        break;
                    case 612000:
                        $this->tra_rsp[$key]['act'] = 'TARTUN GIRO';
                        break;
                    case 621000:
                        $this->tra_rsp[$key]['act'] = 'INQ. SETUN TABUNGAN';
                        break;
                    case 622000:
                        $this->tra_rsp[$key]['act'] = 'INQ. SETUN GIRO';
                        break;
                    case 741000:
                        $this->tra_rsp[$key]['act'] = 'SETUN TABUNGAN';
                        break;
                    case 742000:
                        $this->tra_rsp[$key]['act'] = 'SETUN GIRO';
                        break;
                    case 221000:
                        $this->tra_rsp[$key]['act'] = 'INQ. TRANS TABUNGAN';
                        break;
                    case 222000:
                        $this->tra_rsp[$key]['act'] = 'INQ. TRANS GIRO';
                        break;
                    case 721000:
                        $this->tra_rsp[$key]['act'] = 'TRANSFER TABUNGAN';
                        break;
                    case 722000:
                        $this->tra_rsp[$key]['act'] = 'TRANSFER GIRO';
                        break;
                    case 271000:
                        $this->tra_rsp[$key]['act'] = 'MINI STAT. TABUNGAN';
                        break;
                    case 272000:
                        $this->tra_rsp[$key]['act'] = 'MINI STAT. GIRO';
                        break;
                    case 120000:
                        $this->tra_rsp[$key]['act'] = 'GANTI PIN';
                        break;
                }
                $this->tra_rsp[$key]['id'] = $key+1;
            }
        }
    }

    function format_data() {
        foreach ($this->tra_req as $key => $value) {
            foreach ($value as $id => $data) {
                $this->tra_req['id'] = $key;
                $this->tra_req[$id] = $data;
            }
        }
    }

}
