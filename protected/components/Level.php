<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Level
 *
 * @author Phe
 */
class Level {

    const member = 0;
    const root = 1337;

    public static function getLabel($level) {
        if ($level == self::member)
            return 'User';
        if($level == self::root)
            return 'Administrator';        
        return false;
    }

    public static function getLevelList() {
        return array(
        self::member => 'User',
        self::root => 'Administrator'
        );
    }

}
