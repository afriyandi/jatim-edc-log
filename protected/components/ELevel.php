<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ELevel
 *
 * @author Phe
 */
class ELevel extends CWebUser{
    protected $_model;
    
    function isAdmin(){
        $user = $this->loadUser();
        if($user){
            return $user->level == Level::root;
        }
        
        return false;
    }
    
    function isUser(){
        $user = $this->loadUser();
        if($user){
            return $user->level == Level::member;
        }
        
        return false;        
    }
    
    function getLabelAdmin(){
        if($this->isAdmin() || $this->isUser()){
            return Level::getLabel($this->_model->level);
        }
    }
    
    function loadUser(){
        if($this->_model === null){
            
            $this->_model = Usher::model()->find(array(
                                                    'select' => 'level',
                                                     'condition' => 'name=:name',
                                                     'params' => array(':name' => $this->id)));
        }
        
        return $this->_model;
    }
}
