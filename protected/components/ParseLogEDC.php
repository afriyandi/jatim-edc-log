<?php

class ParseLogEDC extends CModule {

    private $_isonya;
    private $_datanya;
    public $user_req;
    public $trans_req;
    public $rsp_user;
    public $rsp_trans;
    public $reversal;
    public $glob_req;
    public $rsp_glob;

    function __construct() {
        if (!isset(YII::app()->session['phdate'])) {
            YII::app()->session['phdate'] = date('Y-m-j');
        }
        $tanggal = YII::app()->request->getQuery('phdate', YII::app()->session['phdate']);
        $date = strtotime($tanggal);
        $date = addcslashes(date('Y-m-j', $date), '%_');
        YII::app()->session['phdate'] = $tanggal;
        $query = new CDbCriteria(array('condition' => 'TRANSDATETIME LIKE :date',
            'params' => array(':date' => "$date%"),
            'order' => 'TRANSDATETIME desc'));
        $this->_isonya = Extractiso::model()->findAll($query);
        $this->_datanya = $this->wrap_the_iso();
        $this->sort_type();
    }

    private function wrap_the_iso() {
        foreach ($this->_isonya as $key => $value) {
            $bitnya = explode("|", $value->ISOBIT);
            unset($bitnya[0]);
            array_pop($bitnya);
            $datanya = explode("|", $value->ISODATA);
            unset($datanya[0]);
            array_pop($datanya);
            //unset array that has no index due to input dari aziz yang ada delimiter di awal dan akhirnya.
            $combine[$key] = array_combine(array_map(function($str) {
                        return str_replace(" ", "", $str);
                    }, $bitnya), $datanya);
            $combine[$key]['datetime'] = $value->TRANSDATETIME;
        }
        //prevent empty data for that day/
        if (!isset($combine)) {
            $combine = [];
        }

        return $combine;
    }

    private function sort_type() {
        foreach ($this->_datanya as $key => $value) {
            foreach ($value as $index => $isinya) {
                if (strtolower($index) == strtolower('mti')) {
                    switch ($isinya) {
                        case 800:
                            $this->user_req[] = $this->_datanya[$key];
                            $this->glob_req[] = $this->_datanya[$key];
                            break;
                        case 810:
                            $this->rsp_user[] = $this->_datanya[$key];
                            $this->rsp_glob[] = $this->_datanya[$key];
                            break;
                        case 200:
                            $this->trans_req[] = $this->_datanya[$key];
                            $this->glob_req[] = $this->_datanya[$key];
                            break;
                        case 210:
                            $this->rsp_trans[] = $this->_datanya[$key];
                            $this->rsp_glob[] = $this->_datanya[$key];
                            break;
                        case 400:
                            $this->reversal[] = $this->_datanya[$key];
                            break;
                    }
                }
            }
        }
        if (isset($this->glob_req) || isset($this->rsp_glob)) {
            foreach ($this->glob_req as $key => $value) {
                $this->glob_req[$key]['id'] = $key + 1;
            }
            foreach ($this->rsp_glob as $key => $value) {
                $this->rsp_glob[$key]['id'] = $key + 1;
            }
        }
    }

    private function arrayToObject($array) {
        if (is_array($array)) {
            return (object) array_map(array($this, __FUNCTION__), $array);
        } else {
            return (object) $array;
        }
    }

    public function getUserReq() {
        return $this->user_req;
    }

    public function getUserRsp() {
        return $this->rsp_user;
    }

    public function getTransReq() {
        return $this->trans_req;
    }

    public function getTransRsp() {
        return $this->rsp_trans;
    }

}
