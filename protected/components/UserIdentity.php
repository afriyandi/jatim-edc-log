<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    const ERROR_INPUT = 80;

    private $_level;
    private $_id;

    public function authenticate() {
        /*
         * $users=array(
          // username => password
          'demo'=>'demo',
          'admin'=>'admin',
          );
          if(!isset($users[$this->username]))
          $this->errorCode=self::ERROR_USERNAME_INVALID;
          elseif($users[$this->username]!==$this->password)
          $this->errorCode=self::ERROR_PASSWORD_INVALID;
          else
          $this->errorCode=self::ERROR_NONE;
          return !$this->errorCode;
         */
        $user = Usher::model()->find('LOWER(name)=?', array($this->username));
        if ($user == null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if (!$user->validatePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_level = $user->level;
            $this->_id = $user->name;
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode;
    }

    public function getID() {
        return $this->_id;
    }

    public function registeredUser($level) {
        $user = Usher::model()->exists('name = :user_id', array(":user_id" => $this->username));
        if ($user) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
        }

        return $this->errorCode;
    }

    public function registerUsr($level) {
        $hashed = Usher::model()->hashPassword($this->password);
//            $insertUser = Usher::model()->insert('usher', array('name' => $this->username,
//                                                                'pswd' => $hashed,
//                                                                'level' => $level));
        $insertUser = new Usher;
        $insertUser->name = strtolower($this->username);
        $insertUser->pswd = $hashed;
        $insertUser->level = $level;

        if (!$insertUser->save()) {
            $this->errorCode = self::ERROR_INPUT;
        } else {
            $this->errorCode = self::ERROR_NONE;
        }

        return $this->errorCode;
    }

    public function changePass() {
        $hashed = Usher::model()->hashPassword($this->password);
        if(Yii::app()->db->createCommand("UPDATE `extractiso`.`usher` SET `pswd`= '".$hashed."' WHERE `name`='muhaimin'")->execute()){
            return $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode = self::ERROR_PASSWORD_INVALID;
    }

}
