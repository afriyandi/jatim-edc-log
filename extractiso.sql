/*
Navicat MySQL Data Transfer

Source Server         : localboss
Source Server Version : 50525
Source Host           : localhost:3306
Source Database       : extractiso

Target Server Type    : MYSQL
Target Server Version : 50525
File Encoding         : 65001

Date: 2014-03-25 16:24:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `extractiso`
-- ----------------------------
DROP TABLE IF EXISTS `extractiso`;
CREATE TABLE `extractiso` (
  `TRANSDATETIME` datetime DEFAULT NULL,
  `ISOBIT` varchar(2048) DEFAULT NULL,
  `ISODATA` varchar(4096) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of extractiso
-- ----------------------------

-- ----------------------------
-- Table structure for `usher`
-- ----------------------------
DROP TABLE IF EXISTS `usher`;
CREATE TABLE `usher` (
  `name` varchar(15) DEFAULT NULL,
  `pswd` varchar(100) DEFAULT NULL,
  `level` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usher
-- ----------------------------
INSERT INTO `usher` VALUES ('admin', '$2a$13$RUua2/AGylP/FwGv9uLc7.FPhF5r6rbwwVxdj0bLE2xEZRe1BDvuy', '1337');
INSERT INTO `usher` VALUES ('mahmud', '$2a$13$l/LlfM1Ioa928KwrhXAsqOxPtBfhILYOu9ZmCDSoVONjviC6NY3c6', '0');
