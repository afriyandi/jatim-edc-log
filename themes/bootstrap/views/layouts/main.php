<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <?php Yii::app()->bootstrap->register(); ?>
    </head>

    <body>

        <?php
        if(!YII::app()->user->isGuest){
            $datepick = array('class' => 'zii.widgets.jui.CJuiDatePicker',
                    'htmlOptions' => array('class' => 'pull-right .navbar .nav'),
                    'name' => 'datepicker',
                    'value' => date('d F Y', strtotime(YII::app()->session['phdate'])),
                    'options' => array(
                        'showAnim' => 'bounce', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'showButtonPanel' => true,
                        'dateFormat' => 'dd MM yy',
                        'onSelect' => 'js: function(date) { 
    if(date != "") { 
      window.location.href = "' . CHtml::encode($this->createUrl(''
                                        , array('phdate' => ''))) . '"+date ;
    } 
   }',
                    ),);
        } else {
            $datepick = "";
        }
        $this->widget('bootstrap.widgets.TbNavbar', array(
            'fluid' => true,
            'collapse' => true,
            'brand' => CHtml::image(Yii::app()->getBaseUrl() . '/images/BankJatimLogo.png', 'Bank Jatim', array('width' => 90, 'height' => 'auto')),
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'items' => array(
//                        array('label' => 'Home', 'url' => array('index'), 'visible' => Yii::app()->user->isGuest || YII::app()->user->isAdmin()),
                        array('label' => 'Log EDC', 'url' => array('site/logedc'), 'visible' => YII::app()->user->isUser()),
                        array('label' => 'Register User', 'url' => array('site/register'), 'visible' => Yii::app()->user->isAdmin()),
                        array('label' => 'Finger', 'url' => array('finger/index'), 'visible' => Yii::app()->user->isUser()),
                        array('label' => 'Transaction', 'url' => array('transaction/index'), 'visible' => Yii::app()->user->isUser()),
//                        array('label' => 'About', 'url' => array('page', 'view' => 'about')),
//                        array('label' => 'Contact', 'url' => array('contact')),
                    ),
                ),
                $datepick,
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'htmlOptions' => array('class' => 'pull-right'),
                    'items' => array(
                        array('label' => ucfirst(Yii::app()->user->name) . ' | ' . Yii::app()->user->getLabelAdmin(), 'url' => '#', 'items' => array(
                                array('label' => 'Change Password', 'url' => array('site/changepass')),
                                array('label' => 'Logout', 'url' => array('site/logout')),
                            ), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Login', 'url' => array('site/login'), 'visible' => Yii::app()->user->isGuest),
                    ),
                ),
            ),
        ));
        ?>

        <div class="container" id="page">

            <?php if (isset($this->breadcrumbs)): ?>
                <?php
//                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
//                    'links' => $this->breadcrumbs,
//                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?>  by SWI.<br/>
                All Rights Reserved.<br/>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
