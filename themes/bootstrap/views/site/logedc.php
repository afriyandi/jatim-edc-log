<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' - All Log';

if (isset($data->glob_req)) {
    $rqs = new CArrayDataProvider($data->glob_req);
} else {
    $rqs = new CArrayDataProvider(array());
}
?>

<H3>Request</H3>

<?php
if (!isset($rqs)) {
    echo "<H1>Data Not Found</H1>";
}
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered',
    'dataProvider' => $rqs,
    'columns' => array(
        array('name' => 'id', 'header' => 'No'),
        array('name' => 'datetime', 'header' => 'Date Time'),
        array('name' => 'MTI', 'header' => 'MTI'),
        array('name' => '3', 'header' => 'Process Code'),
        array('name' => '41', 'header' => 'TID'),
        array('name' => '42', 'header' => 'MID'),
    ),
));
?>
<H3>Response</H3>
<?php
if (isset($data->rsp_glob)) {
    $rsp = new CArrayDataProvider($data->rsp_glob);
} else {
    $rsp = new CArrayDataProvider(array());
}

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered',
    'dataProvider' => $rsp,
    'columns' => array(
        array('name' => 'id', 'header' => 'No'),
        array('name' => 'datetime', 'header' => 'Date Time'),
        array('name' => 'MTI', 'header' => 'MTI'),
        array('name' => '3', 'header' => 'Process Code'),
        array('name' => '41', 'header' => 'TID'),
        array('name' => '42', 'header' => 'MID'),
        array('name' => '39', 'header' => 'Response Code'),
    ),
));

