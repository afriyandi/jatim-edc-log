<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Register';
?>

<h1>Register User</h1>
<?php
$this->widget('bootstrap.widgets.TbAlert', array('block' => true, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => '&times;', // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array(
            'block' => true, 'fade' => true, 'closeText' => true), // success, info, warning, error or danger
    ),
));
?>

<div class="form">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'register',
        'type' => 'horizontal',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->textFieldRow($model, 'username'); ?>

    <?php echo $form->passwordFieldRow($model, 'password'); ?>

    <?php echo $form->radioButtonListRow($model, 'level', array('User', 'Administrator')); ?>

    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => 'Register',
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
