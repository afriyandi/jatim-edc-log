<H3>Request</H3>
<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name. " - Transaction Transfer Log";

if (isset($data->tra_req)) {
    $rqs = new CArrayDataProvider($data->tra_req);
} else {
    $rqs = new CArrayDataProvider(array());
}
?>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered',
    'dataProvider' => $rqs,
    'template' => "{items}",
    'columns' => array(
        array('name' => 'id', 'header' => 'No'),
        array('name' => 'datetime', 'header' => 'Date Time'),
        array('name' => 'MTI', 'header' => 'MTI'),
        array('name' => '3', 'header' => 'Process Code'),
        array('name' => '41', 'header' => 'TID'),
        array('name' => '42', 'header' => 'MID'),
        array('name' => 'act', 'header' => 'Action'),
    ),
));
?>
<H3>Response</H3>
<?php
if (isset($data->tra_rsp)) {
    $rsp = new CArrayDataProvider($data->tra_rsp);
} else {
    $rsp = new CArrayDataProvider(array());
}

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered',
    'dataProvider' => $rsp,
    'template' => "{items}",
    'columns' => array(
        array('name' => 'id', 'header' => 'No'),
        array('name' => 'datetime', 'header' => 'Date Time'),
        array('name' => 'MTI', 'header' => 'MTI'),
        array('name' => '3', 'header' => 'Process Code'),
        array('name' => '41', 'header' => 'TID'),
        array('name' => '42', 'header' => 'MID'),
        array('name' => 'act', 'header' => 'Action'),
        array('name' => '39', 'header' => 'Response Code'),
    ),
));

